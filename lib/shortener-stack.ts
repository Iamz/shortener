import * as cdk from 'aws-cdk-lib';
import {Construct} from 'constructs';
import * as dynamodb from "aws-cdk-lib/aws-dynamodb";
import * as lambda from "aws-cdk-lib/aws-lambda";
import * as path from "path";
import * as route53 from "aws-cdk-lib/aws-route53";
import * as acm from "aws-cdk-lib/aws-certificatemanager";
import * as api_gateway from "aws-cdk-lib/aws-apigateway";



export class ShortenerStack extends cdk.Stack {
    constructor(scope: Construct, id: string, props?: cdk.StackProps) {
        super(scope, id, props);

        const settingsTable = new dynamodb.Table(this, 'SettingsTable', {
            partitionKey: {
                name: 'name',
                type: dynamodb.AttributeType.STRING
            },
            billingMode: dynamodb.BillingMode.PAY_PER_REQUEST
        });

        const urlTable = new dynamodb.Table(this, 'UrlTable', {
            partitionKey: {
                name: 'id',
                type: dynamodb.AttributeType.STRING
            },
            billingMode: dynamodb.BillingMode.PAY_PER_REQUEST
        });

        const domainName = 's.iamz.io';

        const func = new lambda.Function(this, 'ShortenerFunction', {
            runtime: lambda.Runtime.PYTHON_3_8,
            handler: 'shortener.handler',
            code: lambda.Code.fromAsset(path.join(__dirname, 'lambda-function')),
            environment: {
                SETTINGS_TABLE: settingsTable.tableName,
                URL_TABLE: urlTable.tableName,
                BASE_URL: `https://${domainName}`
            }
        });
        settingsTable.grantFullAccess(func)
        urlTable.grantFullAccess(func)

        const hostedZone = route53.HostedZone.fromHostedZoneAttributes(this, 'HostedZone', {
            hostedZoneId: 'Z2THL4FIV4UEPS',
            zoneName: 'iamz.io.'
        });

        const certificate = new acm.Certificate(this, 'Certificate', {
            validation: acm.CertificateValidation.fromDns(hostedZone),
            domainName
        });

        const api = new api_gateway.LambdaRestApi(this, 'RestApi', {
            handler: func,
            domainName: {
                domainName,
                certificate
            }
        });

        new route53.CnameRecord(this, 'CnameRecord', {
            recordName: domainName,
            domainName: api.domainName?.domainNameAliasDomainName || '',
            zone: hostedZone
        });
    }
}
