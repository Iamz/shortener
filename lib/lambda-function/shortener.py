import os

import boto3

dynamodb = boto3.resource('dynamodb')
settings_table = dynamodb.Table(os.getenv('SETTINGS_TABLE'))
url_table = dynamodb.Table(os.getenv('URL_TABLE'))

# noinspection SpellCheckingInspection
CHARS = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789'
BASE_URL = os.getenv('BASE_URL')


def convert_to_base_62(number):
    arr = []
    while number > 0:
        arr.append(CHARS[number % 62])
        number = int(number / 62)
    arr.reverse()
    return ''.join(arr)


def shorten(url):
    result = settings_table.update_item(
        Key={'name': 'lastId'},
        UpdateExpression='SET #v = #v + :incr',
        ExpressionAttributeNames={'#v': 'value'},
        ExpressionAttributeValues={':incr': 1},
        ReturnValues='UPDATED_NEW'
    )
    new_id = int(result['Attributes']['value'])
    slug = convert_to_base_62(new_id)
    url_table.put_item(
        Item={
            'id': slug,
            'url': url
        }
    )
    return slug


def lookup(slug):
    result = url_table.get_item(
        Key={'id': slug}
    )
    return result['Item']['url']


def handler(event, context):
    path = event['path'][1:]
    if any(c not in CHARS for c in path):
        slug = shorten(path)
        return {
            'statusCode': 200,
            'body': f'{BASE_URL}/{slug}'
        }
    else:
        url = lookup(path)
        return {
            'statusCode': 302,
            'headers': {
                'Location': url
            },
            'body': url
        }
